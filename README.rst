Purpose
=======

This folder structure a minimal setup required to get started with PyPES.
Figures produced by the data analysis software will be namespaced by project and
date produced under ``./figures``.

All files loaded and normalized by the software will be kept in a record and in
NetCDF (H5) format inside ``./cache``.

Finally, you will put your analysis notebooks and your raw data inside separate
projects in ``./datasets``. As an example, you can look at ``./datasets/example``.
In general, the software will look for files in ``./datasets/{project}/data/``.
As a concrete example, the data for the example project can be found in
``./datasets/example/data/``, where there are 5 files with the ``.fits``
extension.

Installation
^^^^^^^^^^^^

Simply clone this repository to wherever you intend to do your analysis work.
This might be in your home or Documents folder, as suits you and your
operating system.

Then, proceed as detailed in the ARPES code repository. In order to let the
analysis code know where you are working, either set the `ARPES_ROOT` environment
variable, or call `arpes.setup(globals(), {string with path to analysis folder})`.
The latter method of calling `arpes.setup` will also perform useful default
imports, as an alternative to configuring an IPython kernel via `.ipython/startup`.

Questions
^^^^^^^^^

If you experience any difficulty, contact Conrad (chstan@berkeley.edu) for help.
